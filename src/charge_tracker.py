import json
import requests
import time
from datetime import datetime
from datetime import date
import csv

# Login to the shipperbee system and get the board data
url_login = 'https://demo-backend.shipperbee.com/api/v1/admin/login'
userName = "tkhan@shipperbee.com"
passWord = "SBTK!234"
r = requests.post(url_login, {"email": userName, "password": passWord, "rememberMe": "true"})
aToken = (json.loads(r.text))["authToken"]

# Name the log according to the day (file will get replaced if you run two logs on the same day)
file_name = f"{str(date.today()).replace('-', '_')}_charge_log.csv"

# Open the file in write mode (overwriting the previous file if it existed, creating new if it didn't)
f = open(file_name, 'w+', newline='')
writer = csv.writer(f)

# Get the mailbox information
url_mail = "https://demo-backend.shipperbee.com/api/v1/mailbox/list"
mailbox_data = requests.get(url_mail, headers={"Authorization": aToken})
mailbox_info = json.loads(mailbox_data.text)["data"]

# Add the header by calling the API and getting all the names of the hives
log_header = ["Date & Time"]
for mailbox in mailbox_info:
    mailbox_name = mailbox['referenceName']
    charge_status = mailbox['batteryStatus']

    if charge_status is not None:
        log_header.append(mailbox_name)

print(log_header)
writer.writerow(log_header)

# Just keep logging
while True:
    # Get the mailbox information
    mailbox_data = requests.get(url_mail, headers={"Authorization": aToken})
    mailbox_info = json.loads(mailbox_data.text)["data"]

    f = open(file_name, 'a', newline='')
    writer = csv.writer(f)

    # Add the appropriate battery statuses to the list for the row
    right_now = datetime.now()
    row = [f"{right_now}"]
    for mailbox in mailbox_info:
        charge_status = mailbox['batteryStatus']

        if charge_status is not None:
            row.append(charge_status)

    # Write the date and charges in the next row using the list
    print(row)
    writer.writerow(row)

    # Close file
    f.close()

    # Don't do anything for 10 mins
    time.sleep(600)





